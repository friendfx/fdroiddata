Categories:Navigation
License:Apache2
Web Site:https://code.google.com/p/osmeditor4android
Source Code:https://code.google.com/p/osmeditor4android/source
Issue Tracker:https://code.google.com/p/osmeditor4android/issues

Auto Name:Vespucci
Summary:OpenStreetMap editor
Description:
* Create and edit new Nodes and Ways
* Append Nodes to existing Ways
* Delete Nodes
* Create, edit and delete Tags
* Download and Upload to OSM-Server
* Highlight unnamed highways, and ways/nodes with TODOs or FIXMEs
* Add, comment and close OpenStreetBugs
* Use a variety of background tile layers as reference
* Show the users GPS-Track with accuracy
* Display the raw data

What is Vespucci NOT?
* a map-view or even a routing-application
* a professional-editing tool like JOSM or Merkaator

Instructions are on the [https://code.google.com/p/osmeditor4android/wiki/Overview?tm=6 wiki].
.

Repo Type:git-svn
Repo:https://osmeditor4android.googlecode.com/svn;trunk=trunk;branches=branches

Build:0.7.0,14
    commit=201

Build:0.8.1r402,17
    commit=404
    target=android-14
    extlibs=android/android-support-v4.jar
    srclibs=ActionBarSherlock@4.1.0
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

Build:0.8.2r416,18
    commit=416
    target=android-14
    extlibs=android/android-support-v4.jar
    srclibs=ActionBarSherlock@4.1.0
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

Build:0.8.3r419,19
    commit=419
    target=android-14
    extlibs=android/android-support-v4.jar
    srclibs=ActionBarSherlock@4.2.0
    prebuild=sed -i 's@\.1=.*@.1=$$ActionBarSherlock$$@' project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

Build:0.9.3r677,25
    commit=0.9/677
    target=android-19
    extlibs=android/android-support-v4.jar
    srclibs=1:ActionBarSherlock@4.4.0
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

Build:0.9.4r681,26
    commit=0.9/681
    target=android-19
    extlibs=android/android-support-v4.jar
    srclibs=1:ActionBarSherlock@4.4.0
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

Build:0.9.4r698,27
    commit=0.9/698
    target=android-19
    extlibs=android/android-support-v4.jar
    srclibs=1:ActionBarSherlock@4.4.0
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

Build:0.9.4r741,28
    commit=0.9/741
    target=android-19
    extlibs=android/android-support-v4.jar
    srclibs=1:ActionBarSherlock@4.4.0
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

Build:0.9.4r745,29
    commit=0.9/745
    target=android-19
    extlibs=android/android-support-v4.jar
    srclibs=1:ActionBarSherlock@4.4.0
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

Build:0.9.4r770,30
    commit=0.9/770
    target=android-19
    extlibs=android/android-support-v4.jar
    srclibs=1:ActionBarSherlock@4.4.0
    prebuild=cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/

Auto Update Mode:None
Update Check Mode:RepoManifest/0.9
Current Version:0.9.4r770
Current Version Code:30

