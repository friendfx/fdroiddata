Categories:Navigation
License:GPLv2
Web Site:http://www.xcsoar.org
Source Code:http://git.xcsoar.org/cgit/master/xcsoar.git
Issue Tracker:http://bugs.xcsoar.org

Auto Name:XCSoar
Summary:Tactical glide computer and maps
Description:
XCSoar is a tactical glide computer for soaring and para glider pilots. It
supports navigation, airspace warnings, final glide calculations, wind
calculation, collision avoidance and many many more features.
.

Repo Type:git
Repo:git://git.xcsoar.org/xcsoar/master/xcsoar.git

Build:6.7.3,92
    commit=v6.7.3
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n ANDROID_SDK=$ANDROID_HOME -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Build:6.7.4,93
    commit=v6.7.4
    subdir=android
    submodules=yes
    output=../output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk
    build=cd .. && \
        for n in 1 2 3; do make TARGET=ANDROIDFAT DEBUG=n TESTING=n ANDROID_SDK=$ANDROID_HOME -j4 output/ANDROIDFAT/bin/XCSoar-release-unsigned.apk && \
        break; done

Auto Update Mode:None
Update Check Mode:Tags
Current Version:6.7.4
Current Version Code:93

