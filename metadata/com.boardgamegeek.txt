Categories:Games
License:GPLv3
Web Site:https://code.google.com/p/boardgamegeek
Source Code:https://code.google.com/p/boardgamegeek/source
Issue Tracker:https://code.google.com/p/boardgamegeek/issues

Auto Name:BoardGameGeek
Summary:Search boardgame data
Description:
This app searches the board game data from boardgamegeek.com. It's not a game
itself — it's a reference tool that provides information about board games.

[https://code.google.com/p/boardgamegeek/wiki/ReleaseNotes Changelog].
.

Repo Type:git-svn
Repo:https://boardgamegeek.googlecode.com/svn;trunk=trunk;tags=tags

Build:3.3,20
    commit=416
    subdir=BoardGameGeek
    target=android-8

Build:3.4,21
    commit=525
    subdir=BoardGameGeek
    target=android-8

Build:3.6,23
    commit=V3.6
    subdir=BoardGameGeek
    target=android-8

Build:4.2,26
    commit=V4.2
    subdir=BoardGameGeek
    target=android-17
    update=.,../ActionBarSherlock

Build:4.3,27
    commit=V4.3
    extlibs=android/android-support-v4.jar
    srclibs=ActionBarSherlock@4.3.1
    prebuild=mv libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
        sed -i 's@\(reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties

Auto Update Mode:None
Update Check Mode:Tags
Current Version:4.3
Current Version Code:27

