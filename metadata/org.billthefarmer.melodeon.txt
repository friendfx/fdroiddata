Categories:Multimedia
License:GPLv3
Web Site:https://github.com/billthefarmer/melodeon/wiki
Source Code:https://github.com/billthefarmer/melodeon
Issue Tracker:https://github.com/billthefarmer/melodeon/issues

Auto Name:Melodeon
Summary:Melodeon emulator for tablets
Description:
Emulates a one row melodeon or a one and a half rown organetto.
There is a choice of midi intruments and keys.

* Keys: Eb, Bb, F, C, G, D, A
* Instruments: Standard midi set, defaults to accordion
* Choice of one row melodeon layout and one and a half row organetto layout
* Choice of fascia images

Uses undocumented built in Sonivox midi synthesizer for midi audio output.
.

Repo Type:git
Repo:https://github.com/billthefarmer/melodeon

Build:1.0,1
    commit=v1.0
    extlibs=arch-arm/libsonivox.so
    scanignore=libs/libsonivox.so
    build=mkdir -p ndk-r8e/toolchains/ && \
        find $$NDK$$ -maxdepth 1 -mindepth 1 -not -name toolchains -print0 | xargs -0 cp -r -t ndk-r8e/ && \
        cp -r $$NDK$$/toolchains/arm-linux-androideabi-4.6/ ndk-r8e/toolchains/ && \
        mv libs/libsonivox.so ndk-r8e/platforms/android-14/arch-arm/usr/lib/ && \
        ./ndk-r8e/ndk-build && \
        rm -rf ndk-r8e/
    buildjni=no

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.01
Current Version Code:101

