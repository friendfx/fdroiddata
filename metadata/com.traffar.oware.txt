Categories:Games
License:GPLv3
Web Site:https://bitbucket.org/przemekr/oware_agg
Source Code:https://bitbucket.org/przemekr/oware_agg/src
Issue Tracker:https://bitbucket.org/przemekr/oware_agg/issues

Auto Name:Oware
Summary:Abstract African board game
Description:
Oware is a board strategy game of African origin. Among its many names are Ayò,
Awalé, Wari, Ouri, Ouril or Uril.

Move Your seeds along the board and grab scores from the opponent pits. Check
[http://en.wikipedia.org/wiki/Oware] for the game rules.

Play with a friend or against a computer player.
.

Repo Type:git
Repo:https://bitbucket.org/przemekr/oware_agg

Build:0.1,1
    commit=0.1
    subdir=android
    init=cd jni/SDL && \
        rm -rf src include SDL-2.0.1* && \
        wget http://libsdl.org/release/SDL2-2.0.1.tar.gz && \
        tar xf SDL2-2.0.1.tar.gz && \
        ln -s SDL2-2.0.1/src . && \
        ln -s SDL2-2.0.1/include .
    buildjni=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.1
Current Version Code:1

