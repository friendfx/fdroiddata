Categories:None
License:GPLv3
Web Site:https://floens.github.io/Clover/
Source Code:https://github.com/Floens/Clover
Issue Tracker:https://github.com/Floens/Clover/issues

Auto Name:Clover
Summary:Browse the popular 4chan image board
Description:
Browser for the popular [https://4chan.org/ 4chan] image board.

Features:
* new post notifications
* smooth image wiewer
* auto thread reload
* HTTPS only
* fast reply 
* 4chan pass support
* pinned threads
* WebM support
* themes (soon) 
.

Repo Type:git
Repo:https://github.com/Floens/Clover.git

Build:v1.0.2,20
    disable=jars included
    commit=v1.0.2
    subdir=Clover

Maintainer Notes:
WARNING: Found JAR file at Clover/docs/ormlite-android-4.48-javadoc.jar
WARNING: Found JAR file at Clover/docs/ormlite-core-4.48-sources.jar
WARNING: Found JAR file at Clover/docs/ormlite-core-4.48-javadoc.jar
WARNING: Found JAR file at Clover/docs/ormlite-android-4.48-sources.jar
WARNING: Found JAR file at Clover/libs/ormlite-core-4.48.jar
WARNING: Found JAR file at Clover/libs/jsoup-1.7.3.jar
WARNING: Found JAR file at Clover/libs/android-support-v13.jar
WARNING: Found JAR file at Clover/libs/ormlite-android-4.48.jar
WARNING: Found JAR file at Clover/libs/httpclientandroidlib-1.1.2.jar
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:v1.0.2
Current Version Code:20

