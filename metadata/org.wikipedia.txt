Categories:Internet
License:GPLv2
Web Site:http://meta.wikimedia.org/wiki/Mobile_Projects
Source Code:https://github.com/wikimedia/WikipediaMobile
Issue Tracker:https://bugzilla.wikimedia.org
Donate:https://donate.wikimedia.org

Auto Name:Wikipedia
Summary:Wikipedia.org client
Description:
Official Wikipedia application.
.

#Releases are in their own branch
Repo Type:git
Repo:https://github.com/wikimedia/WikipediaMobile.git

Build:1.0,1
    commit=v1.0-android

Build:1.0.2,3
    commit=a1dc1d87ac85351f44babbb9101e2311e89924b3

Build:1.0.3,4
    commit=68957ccea4209347833997169f1bcf298beee7d9

Build:1.1.1,11
    commit=9c630ba8b5e65ccd284e8c87501a8475e8bc07c5

Build:1.2,16
    commit=b259ea459bd4494126e582ef5833c87d54a7b595
    submodules=yes

Build:1.2.1,17
    commit=8c03dda8e
    submodules=yes

Build:1.3,20
    commit=178a72271b9f
    submodules=yes

Build:1.3.1,22
    commit=2392caf4
    submodules=yes

Build:1.3.2,23
    commit=b91647636
    submodules=yes

Build:1.3.4,25
    commit=8e7d8a932
    submodules=yes

Build:1.4beta1,21
    disable=Skip beta

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4beta1
Current Version Code:21

