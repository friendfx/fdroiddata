Categories:System
License:GPLv3
Web Site:https://github.com/openintents/aboutapp
Source Code:https://github.com/openintents/aboutapp
Issue Tracker:

Auto Name:OI About
Summary:General app addon
Description:
Some apps can use this app to provide "About" information.
.

Repo Type:git
Repo:https://github.com/openintents/aboutapp.git

Build:1.1,8
    commit=ae20c9d52
    subdir=AboutApp
    target=android-15
    srclibs=OI@4170
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$OI$$/distribution/DistributionLibrary@' project.properties && \
        $$SDK$$/tools/android update project -p $$OI$$/distribution/DistributionLibrary -t android-11 && \
        rm -rf ../promotion ../icons ../AboutAppDemo default.properties

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1
Current Version Code:8

