AntiFeatures:NonFreeNet
Categories:Security
License:GPLv2+
Web Site:https://github.com/cheald/FlexAuth
Source Code:https://github.com/cheald/FlexAuth
Issue Tracker:https://github.com/cheald/FlexAuth/issues

Auto Name:FlexAuth
Summary:Authenticator for Battle.net
Description:
Authenticator for Battle.net
.

Repo Type:git
Repo:https://github.com/cheald/FlexAuth.git

Build:1.1.6,8
    disable=non-free icon used, pull request pending
    commit=17afd40d89597ab7df16ec18f1b1ca02b57ba36f

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.6
Current Version Code:8

