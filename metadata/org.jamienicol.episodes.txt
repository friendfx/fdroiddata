Categories:Multimedia
License:GPLv3+
Web Site:
Source Code:https://github.com/jamienicol/episodes
Issue Tracker:https://github.com/jamienicol/episodes/issues

Auto Name:Episodes
Summary:Track TV shows you have seen
Description:
Keep track of which episodes you've watched of your favourite TV shows.
The data is sourced from [http://thetvdb.com TheTVDB.com], the content of which
is licensed under CC-BY.
.

Repo Type:git
Repo:https://github.com/jamienicol/episodes

Build:0.2,2
    commit=v0.2

Build:0.3,3
    commit=v0.3

Build:0.4,4
    commit=v0.4

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.4
Current Version Code:4

