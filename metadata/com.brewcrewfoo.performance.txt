Categories:System
License:GPLv3
Web Site:http://forum.xda-developers.com/showthread.php?t=2401988
Source Code:https://github.com/horn3t/android_packages_apps_PerformanceControl
Issue Tracker:http://forum.xda-developers.com/devdb/project/?id=600#bugReporter
Donate:http://forum.xda-developers.com/donatetome.php?u=4674443

Auto Name:Performance Control
Summary:System configuration
Description:
* Change CPU Governor, I/O Scheduler and Clock Speed
* Battery information + Fast Charge setting + Battery Life eXtender setting
* Change Voltages Assigned to each CPU frequency
* Customize MinFree Task Killer (set the amount of RAM the minfree taskiller will keep)
* zRam (RAM compression)
* Customize SD read-ahead
* VM Settings such as: Dirty Ratio, Dynamic Fsync, Backlight timeout
* Integrated CPU Spy
* Linux Kernel, CPU, Memory informations
* Misc. Tools: Custom shell command, Wipe cache+dalvik cache, build.prop editor etc.

The sqlite functions won't work unless you have the sqlite3 busybox binary installed.

Requires root, busybox and a custom ROM.
.

Requires Root:Yes

Repo Type:git
Repo:https://github.com/horn3t/android_packages_apps_PerformanceControl

Build:2.1.3,3
    commit=e2b54691bda8a74
    forcevercode=yes

Build:2.1.8,4
    commit=e0ebe71ee3ccd
    prebuild=rm assets/sq*

Build:2.1.8,4
    commit=ca00d73e9f777dc9
    prebuild=rm assets/sq*

Build:2.1.19,2119
    commit=ca00d73e9f777dc909110ffda8b3f1b95e7b3fa8
    prebuild=rm assets/sq*

Maintainer Notes:
Vercodes are rarely updated.
2.1.19 seems to start with a new verCode scheme.
.

Auto Update Mode:None
Update Check Mode:RepoManifest/jb2
Current Version:2.1.19
Current Version Code:2119

