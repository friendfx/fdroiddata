Categories:Multimedia
License:GPLv3+
Web Site:http://opencamera.sourceforge.net
Source Code:http://sourceforge.net/p/opencamera/code
Issue Tracker:http://sourceforge.net/p/opencamera/tickets
Donate:http://sourceforge.net/donate/?user_id=439017
Bitcoin:1LKCFto9SQGqtcvqZxHkqDPqNjSnfMmsow

Auto Name:Open Camera
Summary:Camera App
Description:
A feature rich camera application, including:
* auto-stabilise option
* multitouch zoom
* flash/torch
* choice of focus modes
* face detection
* front/back camera support
* change recording resolution
* video/audio recording
* timer
* burst mode
* silencable shutter
* configurable gui
* geotagging
* external microphone support
* ...
.

Repo Type:git
Repo:git://git.code.sf.net/p/opencamera/code

Build:1.7,9
    commit=v1.7

Build:1.8,10
    commit=v1.8

Build:1.9,11
    commit=v1.9

Build:1.10,12
    commit=v1.10

Build:1.11,13
    commit=v1.11

Build:1.12,14
    commit=v1.12

Build:1.13,15
    commit=v1.13

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.13
Current Version Code:15

