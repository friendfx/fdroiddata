Categories:Children
License:GPLv3+
Web Site:http://androidsoft.org
Source Code:https://androidsoft.googlecode.com/svn/trunk/coloring
Issue Tracker:

Auto Name:Coloring for Kids
Summary:Colouring game for kids
Description:
No description available
.

Repo Type:srclib
Repo:AndroidSoft

Build:1.0.0,1
    commit=50
    subdir=coloring
    target=android-15
    prebuild=rm -rf releases

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0.0
Current Version Code:1

